import React, {useState} from 'react';
import { Button, View, ImageBackground, StyleSheet, Text , SafeAreaView} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagemFundo from './assets/pizza.jpg';
import Login from './src/componentes/Login/login';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import firebase from './src/componentes/servicos/connectionFirebase';
import Produto from './src/componentes/Produtos/produto';
import Menu from './src/componentes/Menu/menu';


const image = { uri: "https://viagemegastronomia.cnnbrasil.com.br/wp-content/uploads/sites/5/2019/04/braz-pizzaria-.jpg" };

const styles = StyleSheet.create({
  login: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    lineHeight: 84,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000c0"
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }

});


function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <ImageBackground source={image} resizeMode="cover" style={styles.image}>
        <Text style={styles.text}>Pizzas</Text>
      </ImageBackground>
    </View>
  );
}


function CadastrarProdutoScreen({ navigation }) {
  return (
    <View style={styles.button}>
        <Produto/>
    </View>
  );
}

function LogarScreen({ navigation }) {
  const [user, setUser] = useState(null);
  if(!user){
    return <Login changeStatus={(user) => setUser(user)}/>
  }
  return (
    <SafeAreaView style={styles.container}>
        <Text></Text>
        <Tab.Navigator>
				<Tab.Screen name="Categorias" component={ListScreenn} />
				<Tab.Screen 
          name="Cadastrar" 
          component={CadastrarProdutoScreen} 
        />
				<Tab.Screen name="Notificações" component={NotificationsScreenn} />
				<Tab.Screen name="Settings" component={SettingsScreenn} />
			</Tab.Navigator>
    </SafeAreaView>
  );
}

function MenuScreen({ navigation }) {
  return (
    <View style={styles.button}>
        <Menu/>
    </View>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {
  const [user,setUser] = useState(null);
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen options={{
          drawerIcon: config => <Icon size={23} name={'home'}></Icon>,
        }} name="Home" component={HomeScreen} />

        <Drawer.Screen options={{
          drawerIcon: config => <Icon size={23} name={'login'}></Icon>,
        }} name="Logar" component={LogarScreen} />

        <Drawer.Screen options={{
          drawerIcon: config => <Icon size={23} name={'login'}></Icon>,
        }} name="Menu" component={MenuScreen} />
      </Drawer.Navigator>      
    </NavigationContainer>
  );
}


const Tab = createBottomTabNavigator();

function ListScreenn() {
	return (
		<View style={styles.container}>
		</View>
	);
}

function PostScreenn() {
	return (
		<View style={styles.container}>

		</View>
	);
}

function NotificationsScreenn() {
	return (
		<View style={styles.container}>
			
		</View>
	);
}

function SettingsScreenn() {
	return (
		<View style={styles.container}>
			
		</View>
	);
}
