import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/database';

let firebaseConfig = {
  apiKey: "AIzaSyDk8SyM48TPLxajDTYFIKDwoNzQUYKIKEk",
  authDomain: "bdguiwall.firebaseapp.com",
  databaseURL: "https://bdguiwall-default-rtdb.firebaseio.com",
  projectId: "bdguiwall",
  storageBucket: "bdguiwall.appspot.com",
  messagingSenderId: "189658477862",
  appId: "1:189658477862:web:0632207ff61aede22c3de7"
};

if (!firebase.apps.length) {
  //inicialize Firebase
  firebase.initializeApp(firebaseConfig);
}

export default firebase;