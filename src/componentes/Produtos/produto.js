import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TextInput, Button, FlatList, ActivityIndicator } from 'react-native';
import firebase from '../servicos/connectionFirebase';
import Icon from 'react-native-vector-icons/Ionicons';
import Listagem from '../Listar/listagem';


export default function App() {
  const [nome, setNome] = useState('');
  const [marca, setMarca] = useState('');
  const [valor, setValor] = useState('');
  const [cor, setCor] = useState('');

  const [produtos, setProdutos] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {

    async function dados() {
      await firebase.database().ref('produtos').on('value', (snapshot) => {
        setProdutos([]);

        snapshot.forEach((chilItem) => {
          let data = {
            key: chilItem.key,
            nome: chilItem.val().nome,
            marca: chilItem.val().marca,
            valor: chilItem.val().valor,
            cor: chilItem.val().cor,
          };

          setProdutos(oldArray => [...oldArray, data].reverse());
        })

        setLoading(false);

      })

    }

    dados();


  }, []);



  async function cadastrar() {
    if (nome !== '' & marca !== '' & valor !== '' & cor !== '') {
      let produtos = await firebase.database().ref('produtos');
      let chave = produtos.push().key;

      produtos.child(chave).set({
        nome: nome,
        marca: marca,
        valor: valor,
        cor: cor
      });

      alert('Produto Cadastrado!');
      setNome('');
      setMarca('');
      setValor('');
      setCor('');
    }
    else{
      if(nome == ''){
      alert('Voce deve informar o nome!');
    }
    if(marca == ''){
      alert('Voce deve informar a marca!');
    }
    if(valor == ''){
      alert('Voce deve informar o valor!');
    }
    if(cor == ''){
      alert('Voce deve informar a cor!');
    }
  }
}

  return (
    <View style={styles.container}>
      <Text style={styles.texto}>Nome</Text>
      <TextInput placeholder='Nome do produto'
        style={styles.input}
        underlineColorAndroid="transparent"
        onChangeText={(texto) => setNome(texto)}
        value={nome}
      />

      <Text style={styles.texto}>Marca</Text>
      <TextInput placeholder='Marca do Produto'
        style={styles.input}
        underlineColorAndroid="transparent"
        onChangeText={(texto) => setMarca(texto)}
        value={marca}
      />

      <Text style={styles.texto}>Valor R$</Text>
      <TextInput placeholder='Valor em R$'
        style={styles.input}
        underlineColorAndroid="transparent"
        onChangeText={(texto) => setValor(texto)}
        value={valor}
      />

      <Text style={styles.texto}>Cor</Text>
      <TextInput placeholder='Cor'
        style={styles.input}
        underlineColorAndroid="transparent"
        onChangeText={(texto) => setCor(texto)}
        value={cor}
      />

      <Button 
        title="Cadastrar"
        onPress={cadastrar}
      />
      <View>
        <Text>
          Listagem de produtos cadastrados
        </Text>
      </View>

      {loading ?
        (
          <ActivityIndicator color="#121212" size={45} />
        ) :
        (
          <FlatList
            keyExtractor={item => item.key}
            data={produtos}
            renderItem={({ item }) => (<Listagem data={item} />)}
          />
        )
      }

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  
  texto: {
    fontSize: 17,
  },

  input: {
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#121212',
    height: 45,
    fontSize: 15
  }
});